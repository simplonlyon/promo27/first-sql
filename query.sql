-- Active: 1695817678749@@127.0.0.1@3306@p27_first

-- Affiche les databases existantes
SHOW DATABASES;
-- Affiche les tables existantes dans la BDD sélectionnée
SHOW TABLES;
-- Affiche la structure d'une table
DESC student;
-- supprime la databse ciblée
-- DROP DATABASE ma_database;
-- Supprime la table ciblée
-- DROP TABLE student;
-- Supprime les données de la table ciblée
-- TRUNCATE TABLE student;
-- Ajouter une nouvelle colonne sur une table existante sans la supprimer
-- ALTER TABLE student ADD COLUMN bloup INT;

SELECT * FROM student;

SELECT name,age FROM student;

SELECT * FROM student WHERE id=2;

SELECT name FROM student WHERE name='Truc' AND age=23;

-- Faire un SELECT qui va récupérer les students qui ont plus de 25 ans
SELECT * FROM student WHERE age > 25;

-- Faire un SELECT qui va récupérer les students qui s'appellent 'Soumiya' (ou le nom en double que vous avez mis)
SELECT * FROM student WHERE name='Soumiya';

-- Faire un SELECT qui va récupérer les students qui s'appellent 'Soumiya' et qui ont moins de 40 ans
SELECT * FROM student WHERE name='Soumiya' AND age < 40;

-- Faire un SELECT qui va récupérer les students qui ne s'appellent pas 'Soumiya' ou qui ont 32 ans
SELECT * FROM student WHERE name!='Soumiya' OR age=32;

-- Faire un SELECT qui va récupérer les students qui ont entre 30 et 40 ans
SELECT * FROM student WHERE age >= 30 AND age <= 40;
SELECT * FROM student WHERE age BETWEEN 30 AND 40;

-- Va chercher tous les student qui contiennent un a dans leur name
SELECT * FROM student WHERE name LIKE "%a%";
-- si on fait LIKE "%a" il ira chercher ceux qui ont le nom qui se termine pas a
-- si on fait LIKE "a%" il ira cherchercher ceux dont le nom commence par a

UPDATE student SET age=25,name='nouvelle valeur' WHERE id=1;

DELETE FROM student WHERE id=2;

-- Une requête avec un LEFT JOIN permettra de récupérer les données de 2 tables
-- simultanément. Il est presque obligatoire que les deux tables ciblées aient une
-- relation via clé étrangère (et on indique dans le ON quelle FK correpond à quelle PK)
SELECT * FROM student 
LEFT JOIN promo ON student.id_promo=promo.id;

-- Le LEFT JOIN ira chercher la table à sa gauche complète et uniquement les données
-- de la table à sa droite qui sont liées à l'autre (donc la requête du dessus va chercher
-- tous les students dont ceux qui n'ont pas de promo, mais pas les promos vides, si on inverse
-- l'ordre des tables dans la requête, ça fera l'inverse toutes les promos dont les vides, mais pas les student sans promo)

-- ces deux requêtes font exactement la même chose
--SELECT * FROM promo LEFT JOIN student ON student.id_promo=promo.id;
--SELECT * FROM student RIGHT JOIN promo ON student.id_promo=promo.id;


-- n'affichera que les students qui ont une promo et les promos qui ont des students
SELECT * FROM student 
INNER JOIN promo ON student.id_promo=promo.id;

-- on peut rajouter des where après la jointure comme avec une requête classique
SELECT * FROM student 
LEFT JOIN promo ON student.id_promo=promo.id
WHERE student.age > 30;$


-- Faire une requête avec jointure pour récupérer les student et le référentiel de leur promo spécifiquement
SELECT student.*, promo.referentiel FROM student
LEFT JOIN promo ON student.id_promo=promo.id;

-- Faire une requête avec jointure pour récupérer toutes les promos qui contiennent un student qui s'appelle Soumiya
SELECT DISTINCT promo.* FROM promo
LEFT JOIN student ON student.id_promo=promo.id
WHERE student.name='Soumiya';

-- Faire une requête avec jointure pour les students dont la promo a commencé il y a au moins un an
SELECT student.* FROM student
LEFT JOIN promo ON student.id_promo=promo.id
WHERE promo.start <= '2023-01-01';
-- Ou bien, une version qui marche quelque soit la date actuelle
SELECT student.* FROM student
LEFT JOIN promo ON student.id_promo=promo.id
WHERE promo.start <= NOW() - INTERVAL 1 YEAR;

-- Faire une requête pour récupérer les students sans promo
SELECT * FROM student WHERE id_promo IS NULL;

-- Faire une requête avec double jointure pour récupérer les students et leur skill
SELECT * FROM student
LEFT JOIN student_skill ON student_skill.id_student=student.id
LEFT JOIN skill ON student_skill.id_skill=skill.id;

-- Faire une requête avec double jointure pour récupérer les students qui ont le skill PHP
SELECT * FROM student
LEFT JOIN student_skill ON student_skill.id_student=student.id
LEFT JOIN skill ON student_skill.id_skill=skill.id
WHERE skill.label='Java';