# Premier SQL

Dans ce projet on voit les requêtes et structures de base du langage MySQL (ou MariaDB).

Le fichier [database](database.sql) contient la structure et les insert, un fichier qu'on peut re-exécuter en entier pour remettre la base de données dans un état propre avec un jeu de données propre.

Le fichier [query](query.sql) contient les requêtes d'exemple ou d'exercices.


## Exercices
### Quelques SELECT
1.  Rajouter dans la base de données 5 students, 2 qui ont le même nom ('Soumiya' par exemple) mais pas le même age et 2 qui ont le même age (41 par exemple) mais pas le même nom
2. Faire un SELECT qui va récupérer les students qui ont plus de 25 ans
3. Faire un SELECT qui va récupérer les students qui s'appellent 'Soumiya' (ou le nom en double que vous avez mis)
4. Faire un SELECT qui va récupérer les students qui s'appellent 'Soumiya' et qui ont moins de 40 ans
5. Faire un SELECT qui va récupérer les students qui ne s'appellent pas 'Soumiya' ou qui ont 32 ans
6. Faire un SELECT qui va récupérer les students qui ont entre 30 et 40 ans


### Table promo en OneToMany
1. Faire un CREATE TABLE pour la table promo qui aura un id, un name en varchar et un referentiel en varchar aussi
2. Rajouter aussi un/des INSERT INTO pour en rajouter 2-3
3. Modifier le CREATE TABLE du student pour y rajouter une colonne id_promo en INT
4. Chercher comment déclarer cette colonne id_promo comme étant une foreign key pointant sur l'id de la table promo

### Table skill et de jointure (ManyToMany)
1. Rajouter un create table (et donc un drop table au dessus) pour une table skill qui aura juste un id et un label (en varchar)
2. Rajouter un nouveau create table pour la table de jointure qu'on appelera student_skill (ou skill_student, on s'en fiche un peu) qui aura comme colonne uniquement un id_skill et un id_student
3. Faire en sorte que ces 2 colonnes soient des foreign key et rajouter une ligne pour dire que la primary key est composite (vous pouvez chercher un peu) et composée des deux colonnes

### Requêtes avec jointure(s)
1. Faire une requête avec jointure pour récupérer les student et le référentiel de leur promo spécifiquement
2. Faire une requête avec jointure pour récupérer toutes les promos qui contiennent un student qui s'appelle Soumiya
3. Faire une requête avec jointure pour les students dont la promo a commencé il y a au moins un an
4. Faire une requête pour récupérer les students sans promo
5. Faire une requête avec double jointure pour récupérer les students et leur skill
6. Faire une requête avec double jointure pour récupérer les students qui ont le skill PHP