-- Active: 1695817678749@@127.0.0.1@3306@p27_first

DROP TABLE IF EXISTS student_skill;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS skill;

DROP TABLE IF EXISTS promo;

CREATE TABLE promo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    referentiel VARCHAR(10) NOT NULL,
    start DATE
);

CREATE TABLE skill(
    id INT PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(64) NOT NULL
);


-- Pour un One To Many (ou Many To One, du point de vue du student)
-- La table de student fait référence à la table promo via la colonne id_promo qu'on définit comme FOREIGN KEY
-- on dit à cette clé etrangère id_promo qu'elle fait référence à la table promo et spécifiquement le champ id de cette table
CREATE TABLE student (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    internship BOOLEAN,
    age INT,
    id_promo INT,
    Foreign Key (id_promo) REFERENCES promo(id)
);

-- Par défaut, si on essaye de supprimer une promo qui est référencée par un student on aura une erreur, il faudrait d'abord supprimé les student (ou leur assigner une autre promo) avant de supprimer la promo
-- On peut modifier ce comportement en rajoutant au bout de la ligne foreign key soit
-- ON DELETE CASCADE : ça signifie que si on supprime une promo, tous ses students seront supprimées
-- ON DELETE SET NULL : ça signifie que si on supprime une promo, tout ses students auront leur id_promo mis à NULL (leur id_promo devra donc être NULLABLE)

-- Pour un Many To Many il faudra créer une table de jointure, celle ci ne contiendra
-- que des clés étrangères et servira à faire les liens entre les 2 tables constituant le Many To Many
CREATE TABLE student_skill(
    id_student INT,
    id_skill INT,
    PRIMARY KEY (id_student,id_skill),
    Foreign Key (id_student) REFERENCES student(id) ON DELETE CASCADE,
    Foreign Key (id_skill) REFERENCES skill(id) ON DELETE CASCADE
);

INSERT INTO promo (name,referentiel,start) VALUES 
('ALT6', 'CDA', '2023-03-12'),
('P27', 'DWWM', '2024-01-08'),
('P23', 'DWWM', '2022-11-23'),
('Promo vide', 'CDA', '2024-01-01');

INSERT INTO skill (label) VALUES ('Java'), ('Typescript'), ('CSS'), ('HTML'), ('SQL');


-- Comme la table student contient une foreign key, il faudra lui donner un id de promo existante comme id_promo
INSERT INTO student (name,age, internship, id_promo) VALUES 
('Bobby', 30, TRUE, 1),
('Soumiya', 24, FALSE, 1),
('Soumiya', 41, TRUE, 2),
('Soumiya', 32, TRUE, 2),
('Weijan', 41, TRUE, 2),
('Albert', 15, TRUE, 3),
('Lonely Joe', 20, FALSE, NULL);

INSERT INTO student_skill (id_student,id_skill) VALUES 
(1,1),
(1,5),
(2,2),
(2,3),
(2,4),
(3,3);